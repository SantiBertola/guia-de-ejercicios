#include <stdio.h>
//Funcion del producto de ambas matrices
void producto(int ma1[2][2] , int ma2[2][2]){
	int ma3[2][2];
	
	//Carga de los valores a la tercera matriz del producto de la matriz 1 y 2
	for(int i=0 ; i<2 ; i++){
		for(int r=0 ; r<2 ; r++){
			
			ma3[i][r] = ma1[i][r] * ma2[i][r]; 
			
		}
	}
	
	//Impresion de la matriz3
	printf("\n");
	for(int i=0 ; i<2 ; i++){
		for(int r=0 ; r<2 ; r++){
			
			printf("%d ",ma3[i][r]);
			
		}
		printf("\n");
	}
	
	
}

int main(int argc, char *argv[]) {
	int matriz1[2][2];
	int matriz2[2][2];
	
	//Carga de valores para la matriz1
	for(int i=0 ; i<2 ; i++){
		for(int r=0 ; r<2 ; r++){
			printf("(1)Ingrese un numero para la posicion %d;%d:",i+1,r+1);
			scanf("%d",&matriz1[i][r]);
		}
	}
	
	//Carga de valores para la matriz2
	printf("\n");
	for(int i=0 ; i<2 ; i++){
		for(int r=0 ; r<2 ; r++){
			printf("(2)Ingrese un numero para la posicion %d;%d:",i+1,r+1);
			scanf("%d",&matriz2[i][r]);
		}
	}
	
	//Llamado de la funcion
	producto(matriz1,matriz2);
	return 0;
}

