#include <stdio.h>

int main() {
	int arreglo[10];
	int mayor=-999999999;
	int menor=999999999;
	int suma;
	
	//Carga de valores
	for(int i=0 ; i<10 ; i++){
		
		printf("Ingrese las presiones: ");
		scanf("%d",&arreglo[i]);
		
	}
	
	//Busqueda del mayor y menor
	for(int i=0 ; i<10 ; i++){
		
		if(arreglo[i] > mayor){
			mayor = arreglo[i];
		}
		if(arreglo[i] < menor){
			menor = arreglo[i];
		}
	}
	
	printf("\nMayor: %d", mayor);
	printf("\nMenor: %d\n", menor);
	
	//Ordenamiento
	printf("\nValores ordenados de mayor a menor:\n");
	for (int i = 0; i < 10; i++) {
		for (int j = i + 1; j < 10; j++) {
			if (arreglo[j] > arreglo[i]) {
				int temp = arreglo[i];
				arreglo[i] = arreglo[j];
				arreglo[j] = temp;
			}
		}
		printf("%d ", arreglo[i]);
	}
	printf("\n");
	
	//Promedio
	for(int i=0 ; i<10 ; i++){
		
		suma += arreglo[i];
		
	}
	
	printf("\nPromedio: %d",suma/10);
	
	//Mas cerca del promedio
	
	if(mayor - suma/10 < suma/10 - menor){
		printf("\nEl promedio est� m�s cerca del mayor");
	} else {
		printf("\nEl promedio est� m�s cerca del menor");
	}
	
	return 0;
}

